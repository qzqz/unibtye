#!/usr/bin/env python3
# from file


path_of_tables = "./tables"

# read order first then read file contains tables
order_tables = {0: "standard_table",
                    1: "arabic_table",
                    2: "somali_table",
                    3: "turkish_table"}

dict_tables ={
    0:          '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ \t\n\r\x0b\x0c',
    1:          "ابتثجحخدذرزسشصضطظعغفقكلمنهويءًٌٍَُِّْٰٖٗآٱأإةؤئىـ،؛؟٪٫٬٩٨٧٦٥٤٣٢١٠",
    2:          "𐒀𐒁𐒂𐒃𐒄𐒅𐒆𐒇𐒈𐒉𐒊𐒋𐒌𐒍𐒎𐒏𐒐𐒑𐒒𐒓𐒔𐒕𐒖𐒗𐒘𐒙𐒚𐒛𐒜𐒝𐒠𐒡𐒢𐒣𐒤𐒥𐒦𐒧𐒨𐒩",
    3:          "qwertyuıopğüişlkjhgfdsaQWERTYUıOPĞÜİŞLKJGHFDSaXCVBNMÖÇ.zxcvbnmöç" 
}

# this count start when table 0 end (100) to 128 (half byte)
#standard_table_range = len(dict_tables[0])
escape_char_range = range(1+99, 3+99+1) # escape char
go_to_table_range = range(4+99, 8+99+1) # go to table 1-5
remember_go_to_table_range = range(9+99, 14+99+1) # remember go table 1-5
                
# after standard table index
#range_of_command_byte = len(command_list)+len(dict_tables[0]) 

"""

for programmer only (not used in program)
key_list = ["find in table", "static text", "function"] # 0, 1, 2

"""

#input_text = "اhi مرحبا "+chr(50000)
input_text = "عبد الباري بيحفظ اسراري"+"hi"#+chr(5000)
#non_repeated_text = set(input_text)


class encode_func:
    #@staticmethod
    def generate_key_to_chars(set_to_check, used_dict):

        for table_index, table in dict_tables.items():
            if len(set_to_check) == 0: break # to exit when not char to check
                
            for char_from_table in table:
                if len(set_to_check) == 0: break # to exit when not char to check
                
                if char_from_table in set_to_check:

                    char_index_in_table = (table.index(char_from_table) + 
                                           (128 if table_index != 0 else 0) )
                    
                    # range of standard table is form [0, 127] = 128 index
                    #if table_index != 0: char_index_in_table += 128
                    # 0 for chars not function 1 for function
                    #print("index = ",char_index_in_table, " table = ", table_index)
                    
                    used_dict[char_from_table] = [0, table_index, 
                                                            char_index_in_table
                                                                .to_bytes(1, "big")]
                    set_to_check.remove(char_from_table)
                    #break

    # search char for each table
    #@staticmethod
    def tables_used(text, compress):
        
        used_dict = {}
        to_check = set(text)

        encode_func.generate_key_to_chars(to_check, used_dict)
        # if char not in any table use escape char
        if to_check:
            for not_found_char in to_check:
                # zero for not use func
                # one for use static word 
                # tow for functions

                len_of_chars_in_standard_table = 99 
                byte_of_char = not_found_char.encode()
                used_dict[not_found_char] = [1, 
                    (len(byte_of_char)+
                     len_of_chars_in_standard_table
                     ).to_bytes(1, "big")+ byte_of_char]

        #print(used_dict)
        return used_dict



    #replace old map to new map with respict roules
    def encode( text):
        new_small_dict = encode_func.tables_used(text, compress = 0)
        new_text = b""
        standard_table = 0
        current_table = 1

        for char_before_encode in text:
            #print(char_before_encode, "char_before_encode")
            #print(new_text) 
            key = new_small_dict[char_before_encode]
        
            if key[0] == 0: new_text += key[2] # num of index

            elif key[0] == 1: new_text += key[1] # that mean static str if key[0] == 1

            elif key[0] == 2: pass # if that fucnc

        return new_text

class decode_func:
    #pass  
    
    def convert_list(byte_message):
        
        convert_dict = {} # index: table
        remember_table_dict = {}
        start_range = 0
        text_without_convert = b""

        index = 0
        while index < len(byte_message):
            byte = byte_message[index]
            #print(byte, go_to_table_range, byte in go_to_table_range)
            if byte in go_to_table_range:
                #print("go to table line ----------------------")
                start_range = index
                convert_dict[start_range] = byte-102 # 103 - 108 # -103 to get 1-5
            
            # under test
            # ----------------------------------------------- fix it 
            elif byte in remember_go_to_table_range:
                if remember_table_dict.get(byte): # check if not exist
                    start_range = index+1
                    remember_table_dict[byte] = byte_message[index+1]
                
                # save table to conver dict
                # save byte that rember table of convert
                convert_dict[index] = remember_table_dict[byte]

            else: # if not convert dict
                text_without_convert += byte.to_bytes(1, "big")
            
            index += 1
            
        #for x,y in convert_dict:
        #    print(x, y, "x, y")
        #print(convert_dict, "convert_dict")
        return convert_dict, text_without_convert
        #replace old map to new map with respict roules


    
    def decode(byte_text):

        #new_small_dict = decode_func.tables_used(byte_text)
        convert_dict, text_without_convert = decode_func.convert_list(byte_text)
        
        new_text = ""
        standard_table = 0
        current_table = 1 # remember 5 movements
        count_to_escape = 0

        for index, char_before_decode in enumerate(text_without_convert):
            #print(char_before_decode, "char_before_decode")
            #print(new_text) 
            #key = new_small_dict[char_before_decode]
        
            #print(index, char_before_decode)
            
            #print("char_before_encode :", char_before_decode)
            # escape counte times
            if count_to_escape:
                count_to_escape -= 1
                continue

            # ----------------------------------------------------------------
            # convert current table 
            if index in convert_dict:
                current_table = convert_dict[index]
                #print(current_table, "current_table")
            # ----------------------------------------------------------------
            #print("char_before_decode", char_before_decode)
            if char_before_decode <= 99: # range of chars starndard table
                new_text += dict_tables[standard_table][char_before_decode]
           
            elif char_before_decode >= 128 : new_text += dict_tables[current_table][char_before_decode%128]


            elif char_before_decode > 99 and char_before_decode < 128:
                if char_before_decode in escape_char_range:
                    
                    count_to_escape = char_before_decode - 98 # count of chat to escape after this
                    # start form 2 to 4 bytes 
                    # command starts with index 100 to 102 

                    print(new_text)
                    # add char with multi bytes
                    new_text += chr(
                            int.from_bytes(
                                text_without_convert[index+1: index+1 +count_to_escape], # count from 2 to 4 maximum
                                'big'))
                    count_to_escape = count_to_escape

            #print(new_text, char_before_decode)

        return new_text


#input_text = input("text : ")
    #print(not_found, used_dict)
#table_output = encode_func.tables_used(input_text, compress=0)
text_after_encode = encode_func.encode(input_text)
print(text_after_encode, len(text_after_encode), len(input_text.encode()))

print(decode_func.decode(text_after_encode))
