#!/usr/bin/env python3
# from file


def set_vars():
    global order_tables, dict_tables, command_dict, input_text

    # read order first then read file contains tables
    order_tables = {0: "standard_table",
                        1: "arabic_table",
                        2: "somali_table",
                        3: "turkish_table"}

    dict_tables ={
        0:          '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ \t\n\r\x0b\x0c',
        1:          "ابتثجحخدذرزسشصضطظعغفقكلمنهويءًٌٍَُِّْٰٖٗآٱأإةؤئىـ،؛؟٪٫٬٩٨٧٦٥٤٣٢١٠",
        2:          "𐒀𐒁𐒂𐒃𐒄𐒅𐒆𐒇𐒈𐒉𐒊𐒋𐒌𐒍𐒎𐒏𐒐𐒑𐒒𐒓𐒔𐒕𐒖𐒗𐒘𐒙𐒚𐒛𐒜𐒝𐒠𐒡𐒢𐒣𐒤𐒥𐒦𐒧𐒨𐒩",
        3:          "qwertyuıopğüişlkjhgfdsaQWERTYUıOPĞÜİŞLKJGHFDSaXCVBNMÖÇ.zxcvbnmöç" 
    }

    command_dict = {
        # useing for encode and decode (use bool parameter)

        1: text_function.select_index_not_used_in_same_table_that_useing_now,
        (2,3,4): text_function.use_escsape_byte_char  # that using for escap 2,3 or 4 bytes chars , key only to count command 
        }

    input_text = "اhi مرحبا "+chr(50000)
    #non_repeated_text = set(input_text)

class text_function:

    @staticmethod
    def select_index_not_used_in_same_table_that_useing_now(parameter_list):
        pass


    @staticmethod
    def use_escsape_byte_char(char, encodeing=1):
        if encodeing:
            temp_var = char.encode()
            print( f"{len(temp_var)}+{char_key}")
            return f"{len(temp_var)}+{char_key}"
        pass

     
class encode:
    

    def analyzing_key(key, current_table):
        # if char from table 
        if key[0] == 0:
            # if in standard table 
            #print(key[2].to_bytes(1, "big"))
            #print(key[1])
            if key[1] == 0:
                return key[2].to_bytes(1, "big")

            # else if not in standard table
            return (key[2]+128).to_bytes(1, "big")

        # if function
        else:
            # key 0 for type of func
            # key 1 for func
            # key 2 for paramere of func 1 if there parameter or its new func

            return command_dict[key[1]](key[2])

            
    #replace old map to new map with respict roules
    def encode(used_dict, text):
        new_text = b""
        standard_table = 0
        current_table = 1

        for char_before_encode in text:
            print(char_before_encode, "char_before_encode")
            print(new_text)
            new_text += analyzing_key(used_dict[char_before_encode], current_table)
        

        return new_text



    # only find dict from string
    def generate_key_to_chars(index_table, chars, text):
        to_check = chars
        find = {}

        for index, text_char in enumerate( text ):
            if text_char in chars:
                
                # 0 for chars not function 1 for function
                find[text_char] = [0, index_table, index]

        return find



    # search char for each table
    def tables_used(text, compress):
        to_check = set(text)
        used_table = {}
        

        for table, chars in dict_tables.items():

            if len(to_check) == 0: break

            find_ =  generate_key_to_chars(table, to_check, chars)

            if find_: used_table.update(find_)

            to_check = to_check.symmetric_difference(find_)

        if to_check:
            for not_found_char in to_check:
                # zero for not use func
                # one for use function

                # zero function is find index not used to use it
                # one function is for use escape byte chars
                used_table[not_found_char] = [1, 0 if compress else 1, not_found_char]
        
        #print(used_table)
        return used_table

    """

    def get_not_use_char_index(current_table, used_index):
        #index =
        #used_index = [y for y in x for i, x in used_dict.items()]
        print(used_index)
        for index in range(256):
            for char_key in used_dict[0]:
                if index !=  0:
                    pass

    """

def decode(text):
    pass

set_vars()
    #print(not_found, used_dict)
table_output = tables_used(input_text, compress=0)

print(encode(table_output, input_text))
